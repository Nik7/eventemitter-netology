let chatOnMessage = (message) => {
  console.log(message);
};
let preparingForAnswer = () => {
  console.log('Готовлюсь к ответу');
};
let closeChat = () => {
  console.log('Чат вконтакте закрылся :(');
};

function closeVkChat() {
  // Закрыть вконтакте
  setTimeout( ()=> {
    console.log('Закрываю вконтакте...');
  vkChat.removeListener('message', chatOnMessage);
  }, 10000 );

};

function closeWebinarChat() {
  setTimeout( ()=> {
    console.log('Закрываю webinarChat');
    webinarChat.removeListener('message',chatOnMessage);
  }, 30000 );

};

function closeFacebookChat() {
  // Закрыть фейсбук
  setTimeout( ()=> {
    console.log('Закрываю фейсбук, все внимание — вебинару!');
    facebookChat.removeListener('message', chatOnMessage);
  }, 15000 );	
};

module.exports = {
	chatOnMessage,
	preparingForAnswer,
	closeChat,
	closeWebinarChat,
	closeVkChat,
	closeFacebookChat
}



